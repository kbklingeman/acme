package com.keith.acme.service;

import java.io.Serializable;


public class Card { //implements Serializable {

    //private static final long serialVersionUID = 1L;

    protected String rank;
    protected String suit;

    // needed for Jackson
    public Card() {}

    public Card(String rank, String suit) {
        this.rank = rank;
        this.suit = suit;
    }

    public String getRank() {
        return rank;
    }

    public String getSuit() {
        return suit;
    }


}
