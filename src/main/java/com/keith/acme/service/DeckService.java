package com.keith.acme.service;

import org.springframework.stereotype.Component;

@Component
public class DeckService {

    private  final int MAX_DECKS=64;

    protected final Deck decks[];

    DeckService() {
        decks = new Deck[MAX_DECKS];
        for(int i = 0; i < MAX_DECKS; i++) {
            decks[i] = new Deck();
        }
    }

    public Card dealCard(int gameId) throws Exception {
        checkIfValidGameId(gameId);
        return decks[gameId].dealCard();
    }

    public void returnCard(int gameId, Card card) throws Exception {
        checkIfValidGameId(gameId);
        decks[gameId].returnCard(card);
    }

    public void shuffle(int gameId) throws Exception {
        checkIfValidGameId(gameId);
        decks[gameId].shuffle();
    }

    protected void checkIfValidGameId(int gameId) throws Exception {
        if(gameId < 0 || gameId > MAX_DECKS)
            throw new Exception("Game out of range!");
    }
}
