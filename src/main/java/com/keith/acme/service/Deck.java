package com.keith.acme.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Deck {

    private final List<Card> cards;
    private final String[] ranks = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King", "Ace"};
    private final String[] suits = {"Clubs", "Diamonds", "Hearts", "Spades"};

    public Deck() {
        cards = new ArrayList<>(52);

        for(String suit : suits) {
            for(String rank : ranks) {
                cards.add(new Card(rank, suit));
            }
        }
        shuffle();
    }

    public Card dealCard() throws Exception {
        if(cards.size() == 0) {
            throw new Exception("Empty deck!");
        }

        Card card = cards.remove(0);
        return card;
    }

    public void returnCard(Card card) {
        cards.add(card);
    }

    public void shuffle() {
        Collections.shuffle(cards, new Random());
    }

}
