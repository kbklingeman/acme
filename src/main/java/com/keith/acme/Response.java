package com.keith.acme;

public class Response {
    protected int status;
    protected String reason;

    public Response() {}
    public Response(int status, String reason) {
        this.status = status;
        this.reason = reason;
    }

    public int getStatus() {
        return status;
    }

    public String getReason() {
        return reason;
    }
}
