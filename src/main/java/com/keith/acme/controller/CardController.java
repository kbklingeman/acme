package com.keith.acme.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.keith.acme.Response;
import com.keith.acme.service.Card;
import com.keith.acme.service.DeckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/cards")
public class CardController {

    @Autowired
    private DeckService deckService;

    @Autowired
    private ObjectMapper objectMapper;

    @GetMapping
    public ResponseEntity<String> games() {
        return new ResponseEntity<>("games", HttpStatus.OK);
    }

    @GetMapping(value = "/deal/{gameId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> dealCard(@PathVariable int gameId) {

        String response;
        try {
            Card card = deckService.dealCard(gameId);
            response = objectMapper.writeValueAsString(card);
        } catch(Exception e) {
            response = "{\"exception\": \"" + e + "\"}";
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping(value = "/return-card/{gameId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> returnCard(@PathVariable int gameId,
                                             @RequestBody Card card) {
        String response;
        try {
            deckService.returnCard(gameId, card);
            response = objectMapper.writeValueAsString(new Response(200, "ok"));
        } catch(Exception e) {
            response = "{\"exception\": \"" + e + "\"}";
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/shuffle/{gameId}")
    public ResponseEntity<String> shuffle(@PathVariable int gameId) {

        String response;
        try {
            deckService.shuffle(gameId);
            response = objectMapper.writeValueAsString(new Response(200, "ok"));
        } catch(Exception e) {
            response = "{\"exception\": \"" + e + "\"}";
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
